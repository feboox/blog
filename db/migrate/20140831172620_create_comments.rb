class CreateComments < ActiveRecord::Migration
  def change
	drop_table :comments
    create_table :comments do |t|
      t.integer :post_id
      t.text :body

      t.timestamps
    end
  end
end
